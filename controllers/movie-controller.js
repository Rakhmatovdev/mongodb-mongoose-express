const Movie = require("../models/movie");


const handleError = (res, error) => {
    res.status(500).json({ error });
  }

    // GET
  const getMovies=(req,res)=>{
    Movie
    .find()
    .sort({ title: 1 })
    .then((data) => {
      res
        .status(200)
        .json(data);
    })
    .catch((err) => handleError(res,err))
  }

  // GET by id
const getMovie= (req, res) => {
    Movie
    .findById(req.params.id)
    .then((data) => {
      console.log(data);
      res
        .status(200)
        .json(data);
    })
    .catch((err) => handleError(res,err))}

//DELETE
const deleteMovie= (req, res) => {
    Movie
      .findByIdAndDelete(req.params.id)
      .then((data) => {
        res
          .status(200)
          .json(data);
      })
      .catch((err) => handleError(res,err))  }


//POST
 const postMovie=(req,res)=>{
    const movie= new Movie(req.body)
    movie
    .save()
    .then((result) => {
      console.log(result);
      res
        .status(201)
        .json(result);
    })
    .catch((err) => handleError(res,err))  }

//PATCH
  const patchMovie=(req,res)=>{
    Movie
      .findByIdAndUpdate((req.params.id),req.body)
      .then((result) => {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch((err) => handleError(res,err))  }

  module.exports={getMovies,getMovie,postMovie,patchMovie,deleteMovie}