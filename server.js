const express = require("express");
const mongoose  = require("mongoose");
const movieRoute  = require("./routes/movie-routes");

const PORT = 3001;
const URL = "mongodb+srv://Jasurbek:Jasurbek3@cluster0.kd7sllt.mongodb.net/datas";


const app =  express();
app.use(express.json());
app.use(movieRoute)
mongoose
.connect(URL)
.then(()=>console.log("Connected to MongoDB"))
.catch((error)=> console.log(`DB connection error: ${error}`))

app.listen(PORT, (err) => {
  err ? console.log(err) : console.log(`listening port ${PORT}`);
});
  
 