const express = require("express");
const { getMovies, getMovie, deleteMovie, postMovie, patchMovie } = require("../controllers/movie-controller");

const router=express.Router()


  // GET
  router.get('/data', getMovies);

//GET by ID
  router.get('/data/:id',getMovie);

  //DELETE
  router.delete('/data/:id',deleteMovie);

  //POST
  router.post("/data",postMovie)

  //PATCH update
  router.patch("/data/:id",patchMovie)

  module.exports=router